# DaznVideoStreamer #

## Functional Requirements ##

Build a service in your preferred language that exposes an API which can be
consumed from any client. This service must check how many video streams a
given user is watching and prevent a user watching more than 3 video streams
concurrently.

## Output Package Requirements ##

The solution has to be provided as a Git repository including full commit history. Please follow the
frequent commit practice so that your local repository indicates reasonable milestones of your
implementation.

You are more than welcome to share your repository on a free service such as GitHub or BitBucket.

The repository MUST contain:
- Source code including external libraries
- Installation and deployment instructions
- README file with URL for testing the service online and a brief explanation on the scalability strategy

## Rules ##

If you do not complete the test please indicate how you would intend to finalize in the README.
You can use any library or tool you feel comfortable with but as few frameworks as possible.

## Solution Design ##

An Azure function application which contains 2 Azure functions the first called `ConnectStream` and the second called `DisconnectStream`. The application is deployed within Azure as a Azure Function App. Below details an overview of the architecture and technologies used to achieved the application solution:

MongoDB -> Repository -> Service -> Function Apps (API Endpoints) -> Consume (Client)

- Server Technology - Node.js using Javascript language
- Persistence (Data Store) - MongoDB (uses a separate cluster from MongoDB Cloud Atlas)
- Patterns Used - Composite (Service composition), Singleton (DB instance)
- Tools - Typescript (to improve maintainability)

**Scalability Strategy**

Using Azure functions provides a serverless deployment in a managed environment which means no virtual machines are required to be running for the application operate. Azure function applications are scaled automatically depending on the compute process required i.e. hosted dynamically based on number of requests. One benefit of using serverless cloud capabilities means you're only charged for compute resources when your functions are running. Using a cloud service like Azure functions of AWS lambdas means the cloud provider scales your application based on demand 

## Future Considerations ##

Potentials future improvements:

1. Add a test framework and assertion library like Jest and Chai to validate service methods behave as expect
2. Create API documentation using a tool like Swagger to document API endpoints
3. Improve security by implementation of oAuth or API keys
4. Create a caching layer to reduce number of trips to the database or potentially replace
5. Introduce dependency injection library to aid with testing and management of class instance lifecycles

## Running the application ##

**Prerequisite** : See Configuring the database section

1. Download the source code from the repository at [https://gitlab.com/TechnicalTests/daznvideostreamer](https://gitlab.com/TechnicalTests/daznvideostreamer "Repository")
2. Navigate to the folder location where the source code was downloaded
3. Open a terminal window of choice and run the following command `npm start` (node.js must be installed on the machine), this will pull required packages, build and start the application
4. API endpoints can be accessed at `http://localhost:7071/api/stream`, for connecting to the stream a http GET method must be specified. To disconnect from the stream a http POST method must be specified. This can be achieved using a tool like Postman
5. The application uses query params of `username` and `resource`. See **Users and Resources** section
6. Execute the endpoint using the query params to see the response from the API. See **Available Endpoints** section

## Running the tests ##

Note: Set the `InTestMode` to true in the `local.settings.json` file to prevent a database connection opening when running tests. If this value is not set to false then the tests WILL NOT run, you will encounter a DB undefined error as the DB does not have a mocked instance.

1. Download the source code from the repository at [https://gitlab.com/TechnicalTests/daznvideostreamer](https://gitlab.com/TechnicalTests/daznvideostreamer "Repository")
2. Navigate to the folder location where the source code was downloaded
3. Open a terminal window of choice and run the following command `npm install` (node.js must be installed on the machine)
4. Once packages are installed run `npm run test`

## Configuring the database ##

Assuming you have MongoDb install locally:

1. Create a new database called `video-streamer`
2. Create a new collection in the db called `users`
3. Insert the document json for users found in the `BackupData` folder
4. Create a new collection in the db called `streams`
5. Insert the document json for streams found in the `BackupData` folder

With the database set up you can now connect to the local db instance by setting `InDebugMode` to true in the `local.settings.json` file.

Alternatively, if you do not want to set up a local instance then you may set the `InDebugMode` to false in the `local.settings.json` file to point to the live database.

## Users and Resources ##

The following usernames are available within the database: `admin` | `user1` | `user2`

The following resources are available within the database: `elephants-dream` | `vw-review` | `steel-tears`

## Available Endpoints ##

For testing `ConnectStream` use http `GET` verb

For testing `DisconnectStream` use http `POST` verb

**Local Testing**

`http://localhost:7071/api/stream?username=admin&resource=elephants-dream`

`http://localhost:7071/api/stream?username=admin&resource=vw-review`

`http://localhost:7071/api/stream?username=admin&resource=steel-tears`

`http://localhost:7071/api/stream?username=user1&resource=elephants-dream`

`http://localhost:7071/api/stream?username=user1&resource=vw-review`

`http://localhost:7071/api/stream?username=user1&resource=steel-tears`

`http://localhost:7071/api/stream?username=user2&resource=elephants-dream`

`http://localhost:7071/api/stream?username=user2&resource=vw-review`

`http://localhost:7071/api/stream?username=user2&resource=steel-tears`

**Live Testing**

`https://video-streamer.azurewebsites.net/api/stream?username=admin&resource=elephants-dream`

`https://video-streamer.azurewebsites.net/api/stream?username=admin&resource=vw-review`

`https://video-streamer.azurewebsites.net/api/stream?username=admin&resource=steel-tears`

`https://video-streamer.azurewebsites.net/api/stream?username=user1&resource=elephants-dream`

`https://video-streamer.azurewebsites.net/api/stream?username=user1&resource=vw-review`

`https://video-streamer.azurewebsites.net/api/stream?username=user1&resource=steel-tears`

`https://video-streamer.azurewebsites.net/api/stream?username=user2&resource=elephants-dream`

`https://video-streamer.azurewebsites.net/api/stream?username=user2&resource=vw-review`

`https://video-streamer.azurewebsites.net/api/stream?username=user2&resource=steel-tears`



