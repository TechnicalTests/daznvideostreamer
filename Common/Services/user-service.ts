import { IUserRepository } from "../Interfaces/Repositories/iuser-repository";
import UserRepository from "../Repositories/user-repository";
import { IUserService } from "../Interfaces/Services/iuser-service";
import { IUser } from "../Interfaces/DomainModels/iuser";


export default class UserService implements IUserService {
    constructor() {
        this.userRepository = new UserRepository();
    }

    private userRepository: IUserRepository;

    public async getMany(): Promise<IUser[]> {
        const users: IUser[] = await this.userRepository.getMany();

        return users;
    }

    public async getOne(id: string): Promise<IUser> {
        const user: IUser = await this.userRepository.getOne(id);

        return user;
    }

    public async update(id: string, user: IUser): Promise<boolean> {
        const result: boolean = await this.userRepository.update(id, user);

        return result;
    }
}

