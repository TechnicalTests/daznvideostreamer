import { IStreamRepository } from "../Interfaces/Repositories/istream-repository";
import StreamRepository from "../Repositories/stream-repository";
import { IStreamService } from "../Interfaces/Services/istream-service";
import { IStream } from "../Interfaces/DomainModels/istream";

export default class StreamService implements IStreamService {
    constructor() {
        this.streamRepository = new StreamRepository();
    }

    private streamRepository: IStreamRepository;

    public async getMany(): Promise<IStream[]> {
        const streams: IStream[] = await this.streamRepository.getMany();

        return streams;
    }

    public async getOne(id: string): Promise<IStream> {
        const stream: IStream = await this.streamRepository.getOne(id);

        return stream;
    }

    public async update(id: string, stream: IStream): Promise<boolean> {
        const result: boolean = await this.streamRepository.update(id, stream);

        return result;
    }
}

