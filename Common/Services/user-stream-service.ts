import { IUserStreamService } from "../Interfaces/Services/iuser-stream-service";
import { IUserService } from "../Interfaces/Services/iuser-service";
import { IStreamService } from "../Interfaces/Services/istream-service";
import UserService from "./user-service";
import StreamService from "./stream-service";
import { IUser } from "../Interfaces/DomainModels/iuser";
import { StatusCodes } from "../Enums/statusCodes";
import { IStream } from "../Interfaces/DomainModels/istream";
import { IUserStreamResponse } from "../Interfaces/Models/iuser-stream-response";

export default class UserStreamService implements IUserStreamService {
    constructor() {
        this.userService = new UserService();
        this.streamService = new StreamService();
    }

    private userService: IUserService;
    private streamService: IStreamService;

    public async connectUser(username: string, resource: string): Promise<IUserStreamResponse> {
        const response: IUserStreamResponse = <IUserStreamResponse>{};
        const userResponse: any = await this.validateUser(username);

        if (!userResponse.isValid) {
            return userResponse.response;
        }

        const streamResponse: any = await this.validateStream(resource);

        if (!streamResponse.isValid) {
            return streamResponse.response;
        }

        if (userResponse.user.activeConnections >= 3) {
            response.message = "Cannot access resource, user has hit maximum connection limit";
            response.statusCode = StatusCodes.MethodNotAllowed;
            response.sourceUrl = null;

            return response;
        }

        const updateUserResponse: any = await this.updateUser(userResponse.user, streamResponse.stream, true);

        return updateUserResponse.response;
    }

    public async disconnectUser(username: string, resource: string): Promise<IUserStreamResponse> {
        const response: IUserStreamResponse = <IUserStreamResponse>{};
        const userResponse: any = await this.validateUser(username);

        if (!userResponse.isValid) {
            return userResponse.response;
        }

        const streamResponse: any = await this.validateStream(resource);

        if (!streamResponse.isValid) {
            return streamResponse.response;
        }

        if (userResponse.user.activeConnections === 0) {
            response.message = "User has no active connections";
            response.statusCode = StatusCodes.BadRequest;
            response.sourceUrl = null;

            return response;
        }

        const updateUserResponse: any = await this.updateUser(userResponse.user, streamResponse.stream, false);

        return updateUserResponse.response;
    }

    private async validateUser(username: string): Promise<any> {
        const response: IUserStreamResponse = <IUserStreamResponse>{};
        const users: IUser[] = await this.userService.getMany();

        response.message = "User not found";
        response.statusCode = StatusCodes.NotFound;
        response.sourceUrl = null;

        if (users.length === 0) {
            return {
                response: response,
                isValid: false
            };
        }

        const user: IUser = users.find(x => x.username === username);

        if (!user) {
            return {
                response: response,
                isValid: false
            };
        }

        return {
            isValid: true,
            user: user
        };
    }

    private async validateStream(resource: string): Promise<any> {
        const response: IUserStreamResponse = <IUserStreamResponse>{};

        response.message = "Video stream not found";
        response.statusCode = StatusCodes.NotFound;
        response.sourceUrl = null;

        const streams: IStream[] = await this.streamService.getMany();

        if (streams.length === 0) {
            return {
                response: response,
                isValid: false
            };
        }

        const stream: IStream = streams.find(x => x.resource === resource);

        if (!stream) {
            return {
                response: response,
                isValid: false
            };
        }

        return {
            response: response,
            isValid: true,
            stream: stream
        };
    }

    private async updateUser(user: IUser, stream: IStream, isConnecting: boolean): Promise<any> {
        const response: IUserStreamResponse = <IUserStreamResponse>{};

        response.message = "Internal server error occurred";
        response.statusCode = StatusCodes.InternalServerError;
        response.sourceUrl = null;

        if (!isConnecting && user.activeConnections < 1) {
            return {
                response: response,
                isValid: false
            };
        }

        isConnecting ? user.activeConnections++ : user.activeConnections--;

        const result: boolean = await this.userService.update(user.Id, user);

        if (result) {
            response.message = isConnecting ? "User connected to video stream" : "User disconnected from video stream";
            response.statusCode = StatusCodes.Ok;
            response.sourceUrl = stream.sourceUrl;

            return {
                response: response,
                isValid: true
            };
        }

        return {
            response: response,
            isValid: false
        };
    }
}