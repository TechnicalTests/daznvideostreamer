import { MongoClient } from "mongodb";

class MongoConnection {
    public client: MongoClient;

    public async initializeConnection(): Promise<void> {
        let client: MongoClient;

        try {
            if (process.env.InDebugMode === "true") {
                client = await MongoClient.connect("mongodb://localhost:27017", { useNewUrlParser: true });
            } else {
                client = await MongoClient.connect(process.env.DbUrl, { useNewUrlParser: true });
            }
        } catch (error) {
            console.log("Unable to connect to database");

            throw error;
        }

        this.client = client;

        if (this.client.isConnected) {
            console.log("Successfully connected to database");
        }
    }
}

// returns singleton instance for any imports
export default new MongoConnection();