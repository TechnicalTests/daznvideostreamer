import { IUserStreamResponse } from "../Models/iuser-stream-response";

export interface IUserStreamService {
    connectUser(username: string, resource: string): Promise<IUserStreamResponse>;
    disconnectUser(username: string, resource: string): Promise<IUserStreamResponse>;
}


