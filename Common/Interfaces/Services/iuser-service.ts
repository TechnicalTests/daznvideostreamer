import { IUser } from "../DomainModels/iuser";

export interface IUserService {
    getMany(): Promise<IUser[]>;
    getOne(id: string): Promise<IUser>;
    update(id: string, user: IUser): Promise<boolean>;
}