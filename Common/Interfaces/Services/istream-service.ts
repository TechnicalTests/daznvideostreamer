import { IStream } from "../DomainModels/istream";

export interface IStreamService {
    getMany(): Promise<IStream[]>;
    getOne(id: string): Promise<IStream>;
    update(id: string, user: IStream): Promise<boolean>;
}