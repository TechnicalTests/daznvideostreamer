export interface IStream {
    Id: string;
    sourceUrl: string;
    type: string;
    resource: string;
}