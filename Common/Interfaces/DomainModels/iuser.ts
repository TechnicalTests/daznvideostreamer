export interface IUser {
    Id: any;
    username: string;
    firstname: string;
    lastname: string;
    activeConnections: number;
}