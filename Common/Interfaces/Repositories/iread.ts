export interface IRead<T> {
    find(document: T): Promise<T[]>;
    findOne(id: string): Promise<T>;
  }