import { IStream } from "../DomainModels/istream";

export interface IStreamRepository {
    create(user: IStream): Promise<boolean>;
    update(id: string, user: IStream): Promise<boolean>;
    delete(id: string): Promise<boolean>;
    getMany(): Promise<IStream[]>;
    getOne(id: string): Promise<IStream>;
  }