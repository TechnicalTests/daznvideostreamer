export interface IWrite<T> {
    insert(document: T): Promise<boolean>;
    findOneAndUpdate(id: string, document: any): Promise<boolean>;
    deleteOne(id: string): Promise<boolean>;
  }