import { IUser } from "../DomainModels/iuser";

export interface IUserRepository {
    create(user: IUser): Promise<boolean>;
    update(id: string, user: IUser): Promise<boolean>;
    delete(id: string): Promise<boolean>;
    getMany(): Promise<IUser[]>;
    getOne(id: string): Promise<IUser>;
  }