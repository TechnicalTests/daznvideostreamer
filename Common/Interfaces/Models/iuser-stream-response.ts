import { StatusCodes } from "../../Enums/statusCodes";

export interface IUserStreamResponse {
    statusCode: StatusCodes;
    message: string;
    sourceUrl: string;
}