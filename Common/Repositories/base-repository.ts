import { Db, Collection, InsertOneWriteOpResult, FilterQuery,
    FindAndModifyWriteOpResultObject, ObjectID, DeleteWriteOpResultObject } from "mongodb";
import MongoConnection from "../../Common/mongo-connection";
import { IWrite } from "../Interfaces/Repositories/iwrite";
import { IRead } from "../Interfaces/Repositories/iread";

export abstract class BaseRepository<T> implements IWrite<T>, IRead<T> {
    constructor(collectionName: string) {
        if(process.env.InTestMode === "false") {
            this.initialize(collectionName);
        }
    }

    private database: Db;
    private collection: Collection;

    private initialize(collectionName: string): void {
        this.database = MongoConnection.client.db(process.env.DbDatabase);
        this.collection = this.database.collection(collectionName);
    }

    public async insert(document: T): Promise<boolean> {
        if (!document) {
            throw new Error("Invalid document, can not insert");
        }

        const writeOp: InsertOneWriteOpResult = await this.collection.insert(document);
        const success: boolean =  writeOp.result.ok === 1 && writeOp.result.n > 0;

        return success;

    }

    public async findOneAndUpdate(id: string, document: any): Promise<boolean> {
        if (!document) {
            throw new Error("Invalid document, can not update");
        }

        if (!id) {
            throw new Error("Invalid id, can not update");
        }

        const filter: FilterQuery<any> = { "_id": new ObjectID(id) };
        const result: FindAndModifyWriteOpResultObject<any> = await this.collection
            .findOneAndUpdate(filter, {$set: document}, { upsert: true });

        return result.ok === 0 ? false : true;
    }

    public async deleteOne(id: string): Promise<boolean> {
        if (!id) {
            throw new Error("Invalid id, can not update");
        }

        const filter: FilterQuery<any> = { "_id": new ObjectID(id) };
        const deleteOp: DeleteWriteOpResultObject = await this.collection.deleteOne(filter);

        const success: boolean =  deleteOp.result.ok === 1 && deleteOp.result.n > 0;

        return success;
    }

    public async find(): Promise<T[]> {
        const results: T[] = await this.collection.find().toArray();

        return results;
    }

    public async findOne(id: string): Promise<T> {
        if (!id) {
            throw new Error("Invalid id, can not find document");
        }

        const filter: FilterQuery<any> = { "_id": new ObjectID(id) };
        const result: T = await this.collection.findOne(filter);

        return result;
    }
}