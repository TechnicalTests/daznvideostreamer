import { BaseRepository } from "./base-repository";
import { IStream } from "../Interfaces/DomainModels/istream";
import { Collections } from "../Enums/collections";
import { IStreamRepository } from "../Interfaces/Repositories/istream-repository";

export default class StreamRepository extends BaseRepository<IStream> implements IStreamRepository {
    constructor() {
        super(Collections.Streams);
    }

    public async create(stream: IStream): Promise<boolean> {
        if (!stream) {
            console.log("Stream object is not valid");

            return false;
        }

        try {
            const result: boolean = await this.insert(stream);

            return result;
        } catch (error) {
            console.log("Unable to create stream", error);

            return false;
        }
    }

    public async update(id: string, stream: IStream): Promise<boolean> {
        if (!id) {
            console.log("Id is not valid");

            return false;
        }

        if (!stream) {
            console.log("Stream object is not valid");

            return false;
        }

        const item: any = {
            "sourceUrl": stream.sourceUrl,
            "type": stream.type,
            "resource": stream.resource
        };

        try {
            const result: boolean = await this.findOneAndUpdate(id, item);

            return result;
        } catch (error) {
            console.log("Unable to update stream", error);

            return false;
        }
    }

    public async delete(id: string): Promise<boolean> {
        if (!id) {
            console.log("Id is not valid");

            return false;
        }

        try {
            const result: boolean = await this.deleteOne(id);

            return result;
        } catch (error) {
            console.log("Unable to delete stream", error);

            return false;
        }
    }

    public async getMany(): Promise<IStream[]> {
        try {
            const streams: any[] = await this.find();

            if (!streams) {
                return [];
            }

            const mappedStreams: IStream[] = streams.map((stream): IStream => {
                return {
                    Id: stream._id.toString(),
                    sourceUrl: stream.sourceUrl,
                    type: stream.type,
                    resource: stream.resource
                };
            });

            return mappedStreams;
        } catch (error) {
            console.log("Unable to retrieve streams", error);

            return [];
        }
    }

    public async getOne(id: string): Promise<IStream> {
        try {
            const stream: IStream = await this.findOne(id);

            return stream;
        } catch (error) {
            console.log("Unable to retrieve stream", error);

            return null;
        }
    }
}