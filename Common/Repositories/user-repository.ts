import { BaseRepository } from "./base-repository";
import { IUser } from "../Interfaces/DomainModels/iuser";
import { Collections } from "../Enums/collections";
import { IUserRepository } from "../Interfaces/Repositories/iuser-repository";

export default class UserRepository extends BaseRepository<IUser> implements IUserRepository {
    constructor() {
        super(Collections.Users);
    }

    public async create(user: IUser): Promise<boolean> {
        if (!user) {
            console.log("User object is not valid");

            return false;
        }

        try {
            const result: boolean = await this.insert(user);

            return result;
        } catch (error) {
            console.log("Unable to create user", error);

            return false;
        }
    }

    public async update(id: string, user: IUser): Promise<boolean> {
        if (!id) {
            console.log("Id is not valid");

            return false;
        }

        if (!user) {
            console.log("User object is not valid");

            return false;
        }

        const item: any = {
            "username": user.username,
            "firstname": user.firstname,
            "lastname": user.lastname,
            "activeConnections": user.activeConnections
        };

        try {
            const result: boolean = await this.findOneAndUpdate(id, item);

            return result;
        } catch (error) {
            console.log("Unable to update user", error);

            return false;
        }
    }

    public async delete(id: string): Promise<boolean> {
        if (!id) {
            console.log("Id is not valid");

            return false;
        }

        try {
            const result: boolean = await this.deleteOne(id);

            return result;
        } catch (error) {
            console.log("Unable to delete user", error);

            return false;
        }
    }

    public async getMany(): Promise<IUser[]> {
        try {
            const users: any[] = await this.find();

            if (!users) {
                return [];
            }

            const mappedUsers: IUser[] = users.map((user): IUser => {
                return {
                    Id: user._id.toString(),
                    activeConnections: user.activeConnections,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    username: user.username
                };
            });

            return mappedUsers;
        } catch (error) {
            console.log("Unable to retrieve users", error);

            return [];
        }
    }

    public async getOne(id: string): Promise<IUser> {
        try {
            const user: IUser = await this.findOne(id);

            return user;
        } catch (error) {
            console.log("Unable to retrieve user", error);

            return null;
        }
    }
}