import { expect } from "chai";
import UserService from "../../../Common/Services/user-service";
import sinon = require("sinon");
import { IUserStreamService } from "../../../Common/Interfaces/Services/iuser-stream-service";
import UserStreamService from "../../../Common/Services/user-stream-service";
import { IUserStreamResponse } from "../../../Common/Interfaces/Models/iuser-stream-response";
import { StatusCodes } from "../../../Common/Enums/statusCodes";
import UserRepository from "../../../Common/Repositories/user-repository";
import { IUser } from "../../../Common/Interfaces/DomainModels/iuser";
import { IStream } from "../../../Common/Interfaces/DomainModels/istream";
import StreamRepository from "../../../Common/Repositories/stream-repository";
import StreamService from "../../../Common/Services/stream-service";

let sandbox: sinon.SinonSandbox;

describe("UserStreamService", () => {
    before(() => {
        sandbox = sinon.createSandbox();
    });

    describe("ConnectStream", () => {
        describe("When no users are present in the DB", () => {
            it("Then the correct response is returned", async () => {
                // arrange
                const users: IUser[] = [];

                sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                const expected: IUserStreamResponse = {
                    message: "User not found",
                    sourceUrl: null,
                    statusCode: StatusCodes.NotFound
                };

                const username: string = "unknown-user";
                const resource: string = "unknown-resource";
                const userStreamService: IUserStreamService = new UserStreamService();

                // act
                const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                // assert
                expect(result).to.not.equal(null);
                expect(result).to.deep.equal(expected);
            });
        });

        describe("When users are present in the DB", () => {
            describe("And a unknown username is supplied", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    const expected: IUserStreamResponse = {
                        message: "User not found",
                        sourceUrl: null,
                        statusCode: StatusCodes.NotFound
                    };

                    const username: string = "unknown-user";
                    const resource: string = "unknown-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });

            describe("And a valid username and resource is supplied when the maximum connection is exceeded", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 7, firstname: "test", lastname: "1", username: "user1"
                    }];

                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    const expected: IUserStreamResponse = {
                        message: "Cannot access resource, user has hit maximum connection limit",
                        statusCode: StatusCodes.MethodNotAllowed,
                        sourceUrl: null
                    };

                    const username: string = "user1";
                    const resource: string = "test-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });

            describe("And a valid username and resource is supplied when the update to db is a success", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    sandbox.stub(UserRepository.prototype, "update").callsFake(async () => { return true; });
                    sandbox.stub(UserService.prototype, "update").callsFake(async () => { return true; });

                    const expected: IUserStreamResponse = {
                        message: "User connected to video stream",
                        statusCode: StatusCodes.Ok,
                        sourceUrl: "some-url"
                    };

                    const username: string = "user1";
                    const resource: string = "test-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });

            describe("And a valid username and resource is supplied when the update to db is a failure", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    sandbox.stub(UserRepository.prototype, "update").callsFake(async () => { return false; });
                    sandbox.stub(UserService.prototype, "update").callsFake(async () => { return false; });

                    const expected: IUserStreamResponse = {
                        message: "Internal server error occurred",
                        statusCode: StatusCodes.InternalServerError,
                        sourceUrl: null
                    };

                    const username: string = "user1";
                    const resource: string = "test-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });
        });

        describe("When no streams are present in the DB", () => {
            it("Then the correct response is returned", async () => {
                // arrange
                const stream: IStream[] = [];
                const users: IUser[] = [{
                    Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                }];

                sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return stream; });
                sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return stream; });

                const expected: IUserStreamResponse = {
                    message: "Video stream not found",
                    sourceUrl: null,
                    statusCode: StatusCodes.NotFound
                };

                const username: string = "user1";
                const resource: string = "unknown-resource";
                const userStreamService: IUserStreamService = new UserStreamService();

                // act
                const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                // assert
                expect(result).to.not.equal(null);
                expect(result).to.deep.equal(expected);
            });
        });

        describe("When streams are present in the DB", () => {
            describe("And a unknown resource is supplied", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    const expected: IUserStreamResponse = {
                        message: "Video stream not found",
                        sourceUrl: null,
                        statusCode: StatusCodes.NotFound
                    };

                    const username: string = "user1";
                    const resource: string = "unknown-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });
        });
    });

    describe("DisconnectStream", () => {
        describe("When no users are present in the DB", () => {
            it("Then the correct response is returned", async () => {
                // arrange
                const users: IUser[] = [];

                sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                const expected: IUserStreamResponse = {
                    message: "User not found",
                    sourceUrl: null,
                    statusCode: StatusCodes.NotFound
                };

                const username: string = "unknown-user";
                const resource: string = "unknown-resource";
                const userStreamService: IUserStreamService = new UserStreamService();

                // act
                const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                // assert
                expect(result).to.not.equal(null);
                expect(result).to.deep.equal(expected);
            });
        });

        describe("When users are present in the DB", () => {
            describe("And a unknown username is supplied", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    const expected: IUserStreamResponse = {
                        message: "User not found",
                        sourceUrl: null,
                        statusCode: StatusCodes.NotFound
                    };

                    const username: string = "unknown-user";
                    const resource: string = "unknown-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });

            describe("And a valid username and resource is supplied when no connection are active", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    const expected: IUserStreamResponse = {
                        message: "User has no active connections",
                        statusCode: StatusCodes.BadRequest,
                        sourceUrl: null
                    };

                    const username: string = "user1";
                    const resource: string = "test-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });

            describe("And a valid username and resource is supplied when the update to db is a success", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 2, firstname: "test", lastname: "1", username: "user1"
                    }];

                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    sandbox.stub(UserRepository.prototype, "update").callsFake(async () => { return true; });
                    sandbox.stub(UserService.prototype, "update").callsFake(async () => { return true; });

                    const expected: IUserStreamResponse = {
                        message: "User disconnected from video stream",
                        statusCode: StatusCodes.Ok,
                        sourceUrl: "some-url"
                    };

                    const username: string = "user1";
                    const resource: string = "test-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });

            describe("And a valid username and resource is supplied when the update to db is a failure", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const users: IUser[] = [{
                        Id: 1, activeConnections: 1, firstname: "test", lastname: "1", username: "user1"
                    }];

                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    sandbox.stub(UserRepository.prototype, "update").callsFake(async () => { return false; });
                    sandbox.stub(UserService.prototype, "update").callsFake(async () => { return false; });

                    const expected: IUserStreamResponse = {
                        message: "Internal server error occurred",
                        statusCode: StatusCodes.InternalServerError,
                        sourceUrl: null
                    };

                    const username: string = "user1";
                    const resource: string = "test-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });
        });

        describe("When no streams are present in the DB", () => {
            it("Then the correct response is returned", async () => {
                // arrange
                const stream: IStream[] = [];
                const users: IUser[] = [{
                    Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                }];

                sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return stream; });
                sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return stream; });

                const expected: IUserStreamResponse = {
                    message: "Video stream not found",
                    sourceUrl: null,
                    statusCode: StatusCodes.NotFound
                };

                const username: string = "user1";
                const resource: string = "unknown-resource";
                const userStreamService: IUserStreamService = new UserStreamService();

                // act
                const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                // assert
                expect(result).to.not.equal(null);
                expect(result).to.deep.equal(expected);
            });
        });

        describe("When streams are present in the DB", () => {
            describe("And a unknown resource is supplied", () => {
                it("Then the correct response is returned", async () => {
                    // arrange
                    const streams: IStream[] = [{
                        Id: "1", resource: "test-resource", sourceUrl: "some-url", type: "mp4"
                    }];

                    const users: IUser[] = [{
                        Id: 1, activeConnections: 0, firstname: "test", lastname: "1", username: "user1"
                    }];

                    sandbox.stub(UserRepository.prototype, "find").callsFake(async () => { return users; });
                    sandbox.stub(UserService.prototype, "getMany").callsFake(async () => { return users; });

                    sandbox.stub(StreamRepository.prototype, "find").callsFake(async () => { return streams; });
                    sandbox.stub(StreamService.prototype, "getMany").callsFake(async () => { return streams; });

                    const expected: IUserStreamResponse = {
                        message: "Video stream not found",
                        sourceUrl: null,
                        statusCode: StatusCodes.NotFound
                    };

                    const username: string = "user1";
                    const resource: string = "unknown-resource";
                    const userStreamService: IUserStreamService = new UserStreamService();

                    // act
                    const result: IUserStreamResponse = await userStreamService.disconnectUser(username, resource);

                    // assert
                    expect(result).to.not.equal(null);
                    expect(result).to.deep.equal(expected);
                });
            });
        });
    });

    afterEach(async () => {
        sandbox.restore();
    });
});