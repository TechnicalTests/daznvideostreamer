import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import MongoConnection from "../Common/mongo-connection";
import { IUserStreamService } from "../Common/Interfaces/Services/iuser-stream-service";
import UserStreamService from "../Common/Services/user-stream-service";
import { IUserStreamResponse } from "../Common/Interfaces/Models/iuser-stream-response";

const httpTrigger: AzureFunction = async (context: Context, req: HttpRequest): Promise<void> => {
    // attempt to connect to database
    await MongoConnection.initializeConnection();

    const userStreamService: IUserStreamService = new UserStreamService();

    if (!MongoConnection.client) {
        context.res = {
            status: 503,
            body: "Service Error: Unable to connect to database"
        };
    }

    const username: string = req.query.username;
    const resource: string = req.query.resource;

    const result: IUserStreamResponse = await userStreamService.connectUser(username, resource);

    context.res = {
        status: result.statusCode,
        body: result
    };

    // close database connection
    await MongoConnection.client.close(true);
};

export default httpTrigger;
